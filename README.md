# lego_detection



## Getting started

### Create a docker with tensorflow

If you don't have yet create a docker
```
xhost local:docker
docker run --name="tensorflow_docker" -it --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --device="/dev/video0:/dev/video0" tensorflow/tensorflow:latest
```

If you have already created a docker
```
xhost local:docker
docker start tensorflow_docker
docker exec -it tensorflow_docker bash
```

### Clone git repository 

```
apt-get update
apt-get upgrade
apt install git

cd home
git clone https://gitlab.inria.fr/auctus-team/people/simongervaise/lego_detection.git
```

### Install dependancies

```
source .installation
```

### Launch detection program

```
cd workspace
python detection_lego.py
```

## Common issues

```
docker: Error response from daemon: Conflict. The container name "/tensorflow_docker" is already in use by container
```

You already have a container named tensorflow_docker

You can remove it :

```
docker container rm tensorflow_docker
```

Or use it (see getting started section)

```
docker: Error response from daemon: error gathering device information while adding custom device "/dev/video0": no such file or directory.
```

Your camera is not plugged or plugged on the wrong device
If you camera is on the wrong device, you can check all your device available with :

```
sudo apt-get install v4l-utils
v4l2-ctl --list-devices
```

And then change /dev/video0 by the device shown until it works
