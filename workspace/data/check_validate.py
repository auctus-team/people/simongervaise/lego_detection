import os
import re
import fileinput

source = "/home/simon/stage/labo/tensorflow2/workspace/data/images_v2/image"

xmls = [f for f in os.listdir(source)
          if re.search(r'([a-zA-Z0-9\s_\\.\-\(\):])+(?i)(.xml)$', f)]
          
for x in xmls:
	if os.path.splitext(x)[0][-3:-1] == "_v":
		os.remove(os.path.join(source, x))
	
